module Views exposing (listView)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Palette exposing (colors)


listView : (a -> Element msg) -> (a -> msg) -> List a -> Element msg
listView getLabel getMsg items =
    let
        viewRow item =
            Input.button
                [ width fill
                , paddingXY 20 10
                , mouseOver
                    [ Background.color colors.hover
                    ]
                ]
                { onPress = Just (getMsg item)
                , label = getLabel item
                }
    in
    column
        [ width (fill |> maximum 300)
        , paddingXY 0 5
        , centerX
        , Background.color colors.contentBackground
        , Border.width 2
        , Border.color colors.border
        ]
        (List.map viewRow items)
        |> el
            [ padding 20
            , centerX
            , width fill
            ]
