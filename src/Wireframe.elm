module Wireframe exposing (placeholder)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font


wireframeTheme =
    { bg = rgb 0.9 0.9 0.9
    , frame = rgb 0.5 0.5 0.5
    , text = rgb 0.3 0.3 0.3
    }


placeholder : List (Attribute msg) -> String -> Element msg
placeholder attr name =
    text name
        |> el
            [ Border.rounded 5
            , Border.dotted
            , Border.color wireframeTheme.frame
            , Border.width 2
            , height fill
            , width fill
            , padding 20
            , Background.color wireframeTheme.bg
            , Font.center
            , Font.color wireframeTheme.text
            ]
        |> el attr
