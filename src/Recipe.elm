module Recipe exposing (Recipe, calculateCarbs, gingerScones, noKneadBread)


type alias Recipe =
    { name : String
    , ingredients : List ( Float, Ingredient ) -- Float is amount in grams
    , massOfResultG : Float
    }


type alias Ingredient =
    { name : String
    , carbsPerMass : Float
    }


noKneadBread : Recipe
noKneadBread =
    { name = "No-Knead Bread"
    , massOfResultG = 1415 * (1 - 0.15)
    , ingredients =
        [ ( 762
          , { name = "AP flour"
            , carbsPerMass = 95.4 / 125
            }
          )
        , ( 18
            -- 3tsp
          , { name = "salt"
            , carbsPerMass = 0
            }
          )
        , ( 5
            -- 1tsp
          , { name = "yeast"
            , carbsPerMass = 41 / 100
            }
          )
        , ( 630
          , { name = "water, room temp"
            , carbsPerMass = 0
            }
          )
        ]
    }


gingerScones : Recipe
gingerScones =
    { name = "Ginger Scones"
    , massOfResultG = 170
    , ingredients =
        [ ( 72
          , { name = "AP flour"
            , carbsPerMass = 95.4 / 125
            }
          )
        , ( 15
          , { name = "sugar"
            , carbsPerMass = 2.7 / 3
            }
          )
        , ( 0.75
            -- 1/8tsp
          , { name = "salt"
            , carbsPerMass = 0
            }
          )
        , ( 1
            -- ?
            -- 1/4tsp
          , { name = "baking soda"
            , carbsPerMass = 0 -- ?
            }
          )
        , ( 26
          , { name = "unsalted butter"
            , carbsPerMass = 0.1 / 227
            }
          )
        , ( 14
          , { name = "grated ginger"
            , carbsPerMass = 0 -- ?
            }
          )
        , ( 70
          , { name = "sour cream"
            , carbsPerMass = 2.9 / 100
            }
          )
        ]
    }


calculateCarbs : Float -> Recipe -> Float
calculateCarbs servingWeight recipe =
    let
        carbFromIngredient ( amountG, ingredient ) =
            amountG * ingredient.carbsPerMass

        estimatedFinalWeight =
            recipe.massOfResultG

        totalCarb =
            List.sum <| List.map carbFromIngredient recipe.ingredients
    in
    servingWeight * totalCarb / estimatedFinalWeight
