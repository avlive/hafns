module Palette exposing (colors)

import Element exposing (..)


colors =
    { appBackground = rgb255 250 250 230
    , contentBackground = rgb 1 1 1
    , border = rgb 0.8 0.8 0.7
    , hover = rgb 0.9 0.9 0.8
    }
