module HafnsApp exposing (Model, Msg, init, update, view)

import Browser
import Element exposing (..)
import Element.Background as Background
import Palette exposing (colors)
import Recipe exposing (Recipe)
import Screens.PastExperiments as PastExperiments
import Screens.RecipeDetails as RecipeDetails
import Screens.RecipeLibrary as RecipeLibrary


type alias Model =
    { allRecipes : List Recipe
    , page : Page
    }


type Page
    = PastExperiments
    | RecipeLibrary
    | RecipeDetailsPage RecipeDetails.Model


type alias Flags =
    ()


init : Flags -> ( Model, Cmd Msg )
init () =
    ( { allRecipes =
            [ Recipe.noKneadBread
            , Recipe.gingerScones
            ]
      , page = PastExperiments
      }
    , Cmd.none
    )


type Msg
    = GoToRecipeLibrary
    | GoToRecipeDetails Recipe
    | RecipeDetailsMsg RecipeDetails.Msg


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GoToRecipeLibrary ->
            ( { model | page = RecipeLibrary }
            , Cmd.none
            )

        GoToRecipeDetails recipe ->
            ( { model
                | page =
                    RecipeDetailsPage (RecipeDetails.init recipe)
              }
            , Cmd.none
            )

        RecipeDetailsMsg recipeDetailMsg ->
            case model.page of
                RecipeDetailsPage recipeDetailsModel ->
                    ( { model | page = RecipeDetailsPage (RecipeDetails.update recipeDetailMsg recipeDetailsModel) }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    { title = "Recipe Science!"
    , body =
        [ layout
            [ Background.color colors.appBackground
            ]
          <|
            case model.page of
                PastExperiments ->
                    PastExperiments.view GoToRecipeLibrary

                RecipeLibrary ->
                    RecipeLibrary.view GoToRecipeDetails model.allRecipes

                RecipeDetailsPage recipeDetailsModel ->
                    RecipeDetails.view recipeDetailsModel
                        |> Element.map RecipeDetailsMsg
        ]
    }
