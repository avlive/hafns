module Screens.RecipeDetails exposing (Model, Msg, init, update, view)

import Dict exposing (Dict)
import Element exposing (..)
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html.Attributes
import Recipe exposing (Recipe)


type alias Model =
    { recipe : Recipe
    , servingSizeInput : String
    , ingredientAmounts : Dict Int String
    , sconeSizeInput : String
    , numberOfSconesInput : String
    }


init : Recipe -> Model
init recipe =
    { recipe = recipe
    , servingSizeInput = "100"
    , ingredientAmounts = Dict.empty
    , sconeSizeInput = "100"
    , numberOfSconesInput = "4"
    }
        |> rescaleRecipe


type Msg
    = ChangeServingWeight String
    | ChangeIngredientAmount Int String
    | ChangeSconeSize String
    | ChangeNumberOfScones String


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeServingWeight newInput ->
            { model | servingSizeInput = newInput }

        ChangeIngredientAmount indexToUpdate newInput ->
            let
                recipe =
                    model.recipe

                sugarAmount =
                    String.toFloat newInput

                updateIngredient i ( amount, ingredient ) =
                    ( if i == indexToUpdate then
                        sugarAmount |> Maybe.withDefault amount

                      else
                        amount
                    , ingredient
                    )

                newIngredients =
                    List.indexedMap updateIngredient recipe.ingredients
            in
            { model
                | ingredientAmounts = Dict.insert indexToUpdate newInput model.ingredientAmounts
                , recipe =
                    { recipe
                        | ingredients = newIngredients
                    }
            }

        ChangeSconeSize newInput ->
            { model | sconeSizeInput = newInput }
                |> rescaleRecipe

        ChangeNumberOfScones newInput ->
            { model | numberOfSconesInput = newInput }
                |> rescaleRecipe


rescaleRecipe : Model -> Model
rescaleRecipe model =
    case
        ( String.toFloat model.sconeSizeInput
        , String.toFloat model.numberOfSconesInput
        )
    of
        ( Just sconeSize, Just numberOfScones ) ->
            let
                totalMassOfRecipeResult =
                    model.recipe.massOfResultG

                computeNewAmount i ( originalAmountInRecipe, _ ) =
                    -- TODO: use the currently entered amount values if different from the original recipe amounts
                    let
                        newFlourAmount =
                            originalAmountInRecipe / totalMassOfRecipeResult * (numberOfScones * sconeSize)
                    in
                    ( i, String.fromInt (round newFlourAmount) )

                newIngredientAmounts =
                    model.recipe.ingredients
                        |> List.indexedMap computeNewAmount
                        |> Dict.fromList
            in
            { model
                | ingredientAmounts =
                    Dict.union newIngredientAmounts model.ingredientAmounts
            }

        _ ->
            model


view : Model -> Element Msg
view model =
    column
        [ width (fill |> maximum 300)
        , height fill
        , padding 20
        , spacing 10
        , centerX
        ]
        [ el
            [ centerX
            , Font.bold
            , Region.heading 1
            ]
            (text model.recipe.name)
        , column []
            [ Input.text []
                { onChange = ChangeSconeSize
                , text = model.sconeSizeInput
                , placeholder = Nothing
                , label =
                    Input.labelLeft
                        [ centerY
                        ]
                        (text "Scone size (g)")
                }
            , Input.text []
                { onChange = ChangeNumberOfScones
                , text = model.numberOfSconesInput
                , placeholder = Nothing
                , label =
                    Input.labelLeft
                        [ centerY
                        ]
                        (text "Number of scones")
                }
            ]
        , -- Recipe
          let
            viewIngredientRow i ( initialAmount, ingredient ) =
                Input.text
                    [ width (fillPortion 1)
                    , htmlAttribute (Html.Attributes.style "text-align" "right")
                    ]
                    { onChange = ChangeIngredientAmount i
                    , text =
                        Dict.get i model.ingredientAmounts
                            |> Maybe.withDefault (String.fromFloat initialAmount)
                    , placeholder = Nothing
                    , label =
                        Input.labelRight
                            [ width (fillPortion 4)
                            , centerY
                            ]
                            (text ("(g) " ++ ingredient.name))
                    }
          in
          column []
            (List.indexedMap viewIngredientRow model.recipe.ingredients)
        , -- Serving size input
          Input.text
            []
            { onChange = ChangeServingWeight
            , text = model.servingSizeInput
            , placeholder = Nothing
            , label =
                Input.labelLeft
                    [ centerY ]
                    (text "Serving (g)")
            }
        , -- Computed nutrition info
          case String.toFloat model.servingSizeInput of
            Nothing ->
                none

            Just servingSizeG ->
                let
                    carbsG =
                        round <| Recipe.calculateCarbs servingSizeG model.recipe
                in
                text ("Carbs: " ++ String.fromInt carbsG ++ "g")
        ]
