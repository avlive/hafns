module Screens.RecipeLibrary exposing (view)

import Element exposing (..)
import Recipe exposing (Recipe)
import Views exposing (listView)


view : (Recipe -> msg) -> List Recipe -> Element msg
view goToRecipeDetails recipes =
    listView
        (\recipe -> text recipe.name)
        (\recipe -> goToRecipeDetails recipe)
        recipes
