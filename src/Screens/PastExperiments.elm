module Screens.PastExperiments exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input


view : msg -> Element msg
view goToRecipeLibrary =
    el
        [ padding 20
        , centerX
        ]
    <|
        Input.button
            -- TODO: pick colors that look good
            [ Background.color (rgb 0.7216 0.7882 0.8078)
            , padding 10
            , Border.rounded 5
            , Border.width 2
            , Border.color (rgb 0.1804 0.4431 0.5176)
            , mouseOver
                [ Background.color (rgb 0.3608 0.6275 0.702) ]
            ]
            { onPress = Just goToRecipeLibrary
            , label = text "Recipe Library"
            }
