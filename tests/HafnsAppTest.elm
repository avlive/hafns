module HafnsAppTest exposing (all)

import HafnsApp
import ProgramTest exposing (..)
import Test exposing (..)
import Test.Html.Query as Query
import Test.Html.Selector exposing (..)
import Test.Ui exposing (changeInput, expectInputValue)


type alias ProgramTest =
    ProgramTest.ProgramTest HafnsApp.Model HafnsApp.Msg (Cmd HafnsApp.Msg)


start : ProgramTest
start =
    ProgramTest.createDocument
        { init = HafnsApp.init
        , view = HafnsApp.view
        , update = HafnsApp.update
        }
        |> ProgramTest.start ()


all : Test
all =
    describe "HafnsApp"
        [ test "choosing how much to make" <|
            \() ->
                start
                    |> clickButton "Recipe Library"
                    |> clickButton "Ginger Scones"
                    |> fillIn "" "Scone size (g)" "80"
                    |> fillIn "" "Number of scones" "7"
                    -- 72g Flour -> 170g scone (2x 85g)
                    -- 7x 80g (560g) scone <- 237g flour  (72 / 170 * 560)
                    |> expectInputValue "(g) AP flour" "237"
        , test "See carbs for a serving (by weight)" <|
            -- scenarios/Calculate carbs.md
            \() ->
                start
                    |> clickButton "Recipe Library"
                    |> clickButton "No-Knead Bread"
                    --- 📲 I enter the weight (in grams)
                    |> fillIn "" "Serving (g)" "22"
                    --- 📲 I see the carb amount
                    |> expectViewHas [ text "Carbs: 11g" ]
        , test "adjust ingredient amounts" <|
            \() ->
                start
                    |> clickButton "Recipe Library"
                    |> clickButton "Ginger Scones"
                    |> fillIn "" "Serving (g)" "100"
                    |> ensureViewHas [ text "Carbs: 41g" ]
                    --- 📲 I change the amount of sugar I will in the recipe
                    |> changeInput "35" "(g) sugar" "8"
                    -- TODO: this used to be change 15 -> 8 -- why didn't the 8 change?
                    |> expectViewHas [ text "Carbs: 38g" ]
        , describe "can choose a recipe" <|
            let
                testCanChoose recipeName =
                    test recipeName <|
                        \() ->
                            start
                                |> clickButton "Recipe Library"
                                |> clickButton recipeName
                                |> expectView
                                    (Query.find
                                        [ tag "h1"
                                        , containing [ text recipeName ]
                                        ]
                                        >> Query.has []
                                    )
            in
            [ "Ginger Scones", "No-Knead Bread" ]
                |> List.map testCanChoose
        ]
