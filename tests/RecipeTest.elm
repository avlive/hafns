module RecipeTest exposing (all)

import Expect
import Recipe
import Test exposing (..)


all : Test
all =
    describe "Recipe"
        [ test "calculateCarbs" <|
            \() ->
                Recipe.calculateCarbs
                    22
                    { name = "No-Knead Bread"
                    , massOfResultG = 1415 * (1 - 0.15)
                    , ingredients =
                        [ ( 762
                          , { name = "AP flour"
                            , carbsPerMass = 95.4 / 125
                            }
                          )
                        , ( 630
                          , { name = "water, room temp"
                            , carbsPerMass = 0
                            }
                          )
                        , ( 18
                            -- 3tsp
                          , { name = "salt"
                            , carbsPerMass = 0
                            }
                          )
                        , ( 5
                            -- 1tsp
                          , { name = "yeast"
                            , carbsPerMass = 41 / 100
                            }
                          )
                        ]
                    }
                    |> round
                    |> Expect.equal 11
        ]
