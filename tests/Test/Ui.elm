module Test.Ui exposing (changeInput, ensureInputValue, expectInputValue)

import Expect exposing (Expectation)
import Html.Attributes
import ProgramTest exposing (ensureView)
import Test.Html.Query as Query
import Test.Html.Selector exposing (..)


changeInput : String -> String -> String -> ProgramTest.ProgramTest model msg effect -> ProgramTest.ProgramTest model msg effect
changeInput oldText label newText =
    ensureView
        (Query.find
            [ tag "label"
            , containing [ text label ]
            ]
            >> Query.find
                [ tag "input"
                , attribute (Html.Attributes.value oldText)
                ]
            >> Query.has []
        )
        >> ProgramTest.fillIn "" label newText


expectInputValue : String -> String -> ProgramTest.ProgramTest model msg effect -> Expectation
expectInputValue label expectedValue =
    ensureInputValue label expectedValue
        >> ProgramTest.done


ensureInputValue : String -> String -> ProgramTest.ProgramTest model msg effect -> ProgramTest.ProgramTest model msg effect
ensureInputValue label expectedValue =
    ensureView
        (Query.find
            [ tag "label"
            , containing [ text label ]
            ]
            >> Query.find
                [ tag "input"
                , attribute (Html.Attributes.value expectedValue)
                ]
            >> Query.has []
        )
