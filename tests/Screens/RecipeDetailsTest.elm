module Screens.RecipeDetailsTest exposing (all)

import Element
import ProgramTest exposing (..)
import Recipe
import Screens.RecipeDetails as RecipeDetails
import Test exposing (..)
import Test.Ui exposing (ensureInputValue, expectInputValue)


type alias ProgramTest =
    ProgramTest.ProgramTest RecipeDetails.Model RecipeDetails.Msg ()


start : ProgramTest
start =
    ProgramTest.createSandbox
        { init = RecipeDetails.init Recipe.gingerScones
        , view =
            RecipeDetails.view
                >> Element.layout []
        , update = RecipeDetails.update
        }
        |> ProgramTest.start ()


all : Test
all =
    describe "RecipeDetailsScreen"
        [ describe "inputs show their updated value" <|
            let
                testInputUpdate inputLabel =
                    test inputLabel <|
                        \() ->
                            start
                                |> fillIn "" inputLabel "80"
                                |> expectInputValue inputLabel "80"
            in
            [ "Scone size (g)"
            , "Number of scones"
            , "(g) unsalted butter"
            , "Serving (g)"
            ]
                |> List.map testInputUpdate
        , test "ingredients start scales for the initial recipe size" <|
            \() ->
                start
                    -- preconditions
                    |> ensureInputValue "Scone size (g)" "100"
                    |> ensureInputValue "Number of scones" "4"
                    -- assert
                    |> expectInputValue "(g) AP flour" "169"
        , test "updating scone size rescales the recipe" <|
            \() ->
                start
                    |> fillIn "" "Scone size (g)" "20"
                    |> expectInputValue "(g) AP flour" "34"
        , test "updating number of scones rescales the recipe" <|
            \() ->
                start
                    |> fillIn "" "Number of scones" "2"
                    |> expectInputValue "(g) AP flour" "85"
        , test "changing recipe scale rescales all ingredient" <|
            \() ->
                start
                    |> fillIn "" "Scone size (g)" "20"
                    |> expectInputValue "(g) unsalted butter" "12"
        ]
