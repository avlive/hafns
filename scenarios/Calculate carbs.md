Scenario: know carbs in a slice of homemade bread

> Recipe:
> - 762g AP flour
> - 630g water, room temp
> - 3tsp salt (18g)
> - 1tsp yeast (est 5g)
>
> Ingredients info:
> - AP flour: 125g -> 95.4g carb
> - yeast: 100g -> 41g
> 
> doughWeight = 762g + 630g + 18g + 5g = 1415g
> estimatedBreadWeight = doughWeight * (10% - 20%) = 1202.75g
> totalCarb = (95.4/125)*762 + (41/100)*5 = 583.608
> answer = 22g * (totalCarb / estimatedBreadWeight) = 11g

- I have a no-knead bread recipe
- I mix the ingredients
- I let it rest overnight
- I bake the bread
- I wait for the bread to cool
- I cut a slice of bread
- I want to know how many carbs, so I can dose my insulin
   - I weigh the slice on my kitchen scale
   - 📲 I enter the weight (in grams)  (22g)
   - 📲 I see the carb amount (11g)
   - I enter the carbs (in grams) in my insulin pump
   - I deliver the insulin bolus that the pump calculated based on the carb amount


Scenario: customizing the ingredients in scones

> With 15g sugar (the default):
> doughWeight = 26 + 72 + 0.75 + 1 + 15 + 70 + 14 = 198.75
> estimatedFinal = doughWeight * (1 - 15%) = 168.94
> totalCarb = 26*(0.1/227) + 72*(95.4/125) + 15*(2.7/3) + 70*(2.9/100) = 70.49
> answer =  100g * (totalCarb / estimatedFinal) = 41.72
>
> With 8g sugar:
> doughWeight = 26 + 72 + 0.75 + 1 + 8 + 70 + 14 = 191.75
> estimatedFinal = doughWeight * (1 - 15%) = 162.9875
> totalCarb = 26*(0.1/227) + 72*(95.4/125) + 8*(2.7/3) + 70*(2.9/100) = 64.19185
> answer =  100g * (totalCarb / estimatedFinal) = 39

- I have a scone recipe
- I want to reduce the amount of sugar in the recipe
- 📲 I change the amount of sugar I will in the recipe (8g)
- I mix the ingredients
- I bake the scones
- I take a scone
- I want to know how many carbs, so I can dose my insulin
   - I weigh the scone on my kitchen scale
   - 📲 I enter the weight (in grams) (100g)
   - 📲 I see the carb amount (39g)
   - I enter the carbs (in grams) in my insulin pump
   - I deliver the insulin bolus that the pump calculated based on the carb amount
